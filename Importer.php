<?php


class ViraniImporter {

  /*
   * $path is path of csv file
   * $file data is csv imported data
   *
   */
  public $path;

  public $file_data;

  public $taxonomy_id;

  /**
   * Importer constructor.
   */
  function __construct($taxonomy_id, $file_path) {
    $this->path = $file_path;
    $this->taxonomy_id = $taxonomy_id;
    add_action("init", [$this, "get_csv_file_data"]);
  }

  /**
   * get csv file data
   */
  function get_csv_file_data() {

    $csv = array_map('str_getcsv', file($this->path));
    $result = [];
    foreach ($csv as $key => $item) {
      foreach ($item as $child_key => $child_item) {
        if ($child_item != "") {
          $result[$key][] = $child_item;
        }
      }
      if (sizeof($result[$key]) < 3) {
        unset($result[$key]);
      }
      if ($result[$key][0] == "AREA") {
        unset($result[$key]);
      }
    }
    $this->file_data = array_values($result);

    $this->updateQneighbourhoodTaxonomy($this->file_data);

  }

  function updateQneighbourhoodTaxonomy($data) {


    foreach ($data as $term_info) {
      foreach ($term_info as $key => $term) {

        if ($key == 2) {

          $parent_term_id = $this->getParentTermIdByName($term);
          update_term_meta($parent_term_id, "query_string", $term_info[0]);
          update_term_meta($parent_term_id, "area_code", $term_info[0]);
        }

        if ($key == 3) {

          $child_term_id = $this->getChildTermById($term, $parent_term_id);
          $area = isset($term_info[1]) ? $term_info[1] : "";
          $sub_area = isset($term_info[0]) ? $term_info[0] : "";
          update_term_meta($child_term_id, "query_string", $area);
          update_term_meta($child_term_id, "area_code", $sub_area);

        }

      }
    }
  }


  public function getParentTermIdByName($term) {
    $parent_term_id = isset($all_exist_term[strval($term)]) ? $all_exist_term[strval($term)] : "";
    if (empty($parent_term_id)) {
      $term_exist = term_exists(strval($term), $this->taxonomy_id, 0);
      // create term if not exist
      if ($term_exist == NULL || $term_exist == 0) {
        $insert_parent_term_res = wp_insert_term(
          trim($term),   // the term
          $this->taxonomy_id // the taxonomy
        );
        $parent_term_id = $insert_parent_term_res['term_id'];
      }
      else {
        $parent_term_id = $term_exist['term_id'];
      }
      $all_exist_term[strval($term)] = $parent_term_id;
    }

    return $parent_term_id;
  }

  public function getChildTermById($term, $parent_term_id) {
    $child_term_exist = term_exists(strval($term), $this->taxonomy_id, $parent_term_id);
    // create term if not exist
    if ($child_term_exist == NULL || $child_term_exist == 0) {
      $insert_child_term_res = wp_insert_term(
        $term,
        $this->taxonomy_id,
        [
          "parent" => $parent_term_id,
        ]
      );
      $child_term_id = $insert_child_term_res['term_id'];
    }
    else {
      $child_term_id = $child_term_exist['term_id'];
    }

    return $child_term_id;
  }

}